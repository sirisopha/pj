$(document).ready(function() {
    var account = {
        '1234' : {'pin': '1234', 'balance': 500},
		'5678' : {'pin': '5678', 'balance': 1500},
		'0000' : {'pin': '0000', 'balance': 2500}
	}

	function check(form){
        var accountNo = form.inputUser.value;
        var pin = form.inputPassword.value;
        
        if(accountNo != null && account[accountNo]['pin'] == pin){
			return true;
        }else{
            alert("เลขบัญชีหรือรหัสผ่านไม่ถูกต้อง!");
            return false;
        }
    }
});

